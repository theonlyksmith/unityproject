﻿using UnityEngine;
using System.Collections;

public class playerTracker : MonoBehaviour {

    //location of kirby
    public Transform player;
    public Vector2 offset;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        //resets the camera to wherever kirby is with an offset
        transform.position = new Vector2(player.position.x + offset.x, player.position.y + offset.y); 
	}
}
