﻿using UnityEngine;
using System.Collections;

public class kirbyMover : MonoBehaviour {

	//checks which way kirby is facing
	public bool facingRight = true;

	//click and drag rigidbody from unity hierachy
	public Rigidbody2D rb;

	//initialize ground check
	public bool isGrounded;
	public LayerMask isGroundLayer;
	public Transform groundCheck;

	//variable used to check if kirby has sucked in air or not
	int isPoofy = 0;


    /* DOM
     The animations do not run currently. The code and triggers all function,
     but the dopesheet is failing to find the sprites, despite displaying them 
     when clicked on. The only thing i could thing of as that all my assets are
     scaled up 4X and that was messing it up somehow, but accounting for this 
     in the dope sheet just inflated the sprites, and they still would be 
     considered missing. If you know what I fucked up, then please give me a heads-up.
     
     I also freely admit that the camera does not display correctly and the teleports
     through the level stages do not work, and this is because i have no idea how to do it properly. 
     No amount of google-ing showed me how to solve either problem, and I figured handing in something 
     that has some of the pieces set would be better than waiting until it all worked and handing in
     nothing on time. I will re-submit it when I get some feedback and learn how to fix it.
     */

	//animation intializer/ check variables
	Animator animController;

	int idleAnim = 1;
	int jumpAnim = 0;
	int flyAnim = 0;
	int suckAnim = 0;
	int shootAir = 0;
	int shootGround = 0;

    /*//teleport trigger variables
    public Transform other;
    public Transform enterMinibossArea;
    public Transform enterCavesArea;
    public Transform enterFinalCave;*/

	// Use this for initialization
	void Start () {
	
		//loads the animator
		animController = GetComponent<Animator> ();
		if (!animController) {
			Debug.LogError("No animator loaded.");
		}
	}
	
	// Update is called once per frame
	void Update () {
	
		//checks if kirby is on the ground
		isGrounded = Physics2D.OverlapCircle (groundCheck.position, 0.2f, isGroundLayer);

		//jump
		if (Input.GetKeyDown (KeyCode.Space) && isGrounded) {
			rb.AddForce (Vector2.up * 6, ForceMode2D.Impulse);
		}

		//jump animation check (NEED TO ADD POOFY CHECK)
		if (!isGrounded) {
			jumpAnim = 1;
		} 
		else
			jumpAnim = 0;

        //sucking animation trigger
        if (Input.GetKey(KeyCode.DownArrow))
        {
            suckAnim = 1;
        }
        else
            suckAnim = 0;

		//horizontal movement
		float moveValue = Input.GetAxisRaw ("Horizontal");
		
		//displays values for moveValue
		Debug.Log ("moveValue: " + moveValue);
		
		//flip triggers on moveValue
		if (moveValue > 0 && !facingRight) {
			FlipHorizontal ();
		} 
		else if (moveValue < 0 && facingRight)
			FlipHorizontal ();

		//moves kirby as long as he isnt sucking
		if (suckAnim == 0) {
			rb.velocity = new Vector2 (moveValue * 2, rb.velocity.y);
		}

		//changes animation to run if kirby is moving but not jumping
		if (jumpAnim == 0) {
			animController.SetFloat ("Walk", Mathf.Abs (moveValue));
		}

		animController.SetFloat ("Jump", jumpAnim);

		animController.SetFloat ("Suck", suckAnim);
	}

	//flips kirby horizontally
	void FlipHorizontal(){
		facingRight = !facingRight;
		
		Vector3 flippyWippy = transform.localScale;
		flippyWippy.x *= -1;
		transform.localScale = flippyWippy;
	}

    //teleports kirby to the next piece of the level when collider trigger is hit
   /* void OnTriggerEnter(Collision2D WarpStar) {
        
        if (other.tag == "Player") {
            other.transform.position = enterMinibossArea.position;
        }
    }

    void OnTriggerEnter(Collision2D exitMinibossArea){

        if (other.tag == "Player")
        {
            other.transform.position = enterCavesArea.position;
        }
    }

    void OnTriggerEnter(Collision2D exitCavesArea){

        if (other.tag == "Player")
        {
            other.transform.position = enterFinalCave.position;
        }
    }*/
}
