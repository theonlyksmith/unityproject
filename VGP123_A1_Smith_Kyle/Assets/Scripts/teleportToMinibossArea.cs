﻿using UnityEngine;
using System.Collections;

public class teleportToMinibossArea : MonoBehaviour {

    public Transform other;
    public Transform enterMinibossArea;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    //teleports kirby to the next piece of the level when collider trigger is hit
    void OnTriggerEnter(Collision2D c)
    {

        if (other.tag == "Player")
        {
            other.transform.position = enterMinibossArea.position;
        }
    }
}
